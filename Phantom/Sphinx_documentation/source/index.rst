.. phantom documentation master file, created by
   sphinx-quickstart on Tue Apr 20 09:57:53 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the python analysis of computed tomography's documentation
######################################################################


.. rubric:: Context :

The challenge of this project is to develop a program that is able to analyse tomography images in an autonomus way.
PHANTOM (PytHon ANalysis of computed TOMography) is a program that can segmente different materials displayed on an image.
From this segmentation it is possible to obtain the objects' shape in STL file and display the render as the program compute. There are plenty of differents options available.
For this reason a GUI (Graphical User Interface) has been developped with Qt Designer to regroup those options and make it user-friendlier.
This "Sphinx" documentation will explain how those options work and gives advices when to use them.

.. contents:: PHANTOM's Table of Contents


Ui_MainWindow Class
###################

************
Intoduction
************

The PHANTOM GUI was designed to help the user parametrize the study he wants to elaborate.
Qt Designer is a software enabling creation of a GUI and multiple other options with PyQt5 (python library),
such as connections between parameters, execute an action after a button pushed or disabling certain items if the input information are not correct.

.. image:: GUI.png
   :align: center
   :width: 700
   :alt: alternate text

************
Input/Output
************

In order to fill in the path of your 16bit .tif images folder you will need to select the "Input" button in the upper right corner of the IO group box.
A navigation window will open enabling the selection of a folder.
Warning : if you select a folder containing something else than 16bit .tif images the program won't compute correctly.

Repeat the same process for the output path (no specific requirement)

.. image:: IO.png
   :align: center
   :alt: alternate text

**********
Parameters
**********

In this tab group box you will be able to set the desired parameters for the study.

.. image:: Parameters.png
   :align: center
   :alt: alternate text

========
Method
========

.. image:: Method.png
   :align: center
   :alt: alternate text

.. image:: Method1.png
   :align: center
   :alt: alternate text

The thresholding *Segmentation = 'without overlap'* refers to the way of thresholding at the minimum of the frame's gray level histogram function. A pixel can only be part of a single material.

When to use the 'without overlap' method :

* Quickest compute time
* Keep the picture in 8bit white/black
* Allows 3D connectivity module
* Allows STL file creation and display

The thresholding *Segmentation = 'with overlap'* refers to the way of thresholding at the zero value of the minimum and maximum function of the frame's gray level histogram. A pixel can be part of several materials.

When to use the 'with overlap' method :

* Close density materials captured
* Change the picture in 8bit Red/Green/Blue
* Show common pixel in red
* DO NOT USE when the number of minimum and maximum pixels are too close

.. image:: Common.png
   :align: center
   :alt: alternate text

The thresholding *Histogram threshold = 'Global'* refers to the way of thresholding the function of the stack frames' gray level histogram. All the pictures are summed together to get a single histogram.

When to use the 'Global' method :

* Quickest compute time
* Few materials
* Few disappearance of material in the stack of frames



The thresholding *Histogram threshold = 'image per image'* refers to the way of thresholding the function of each frame's gray level histogram. The picture's gray level histogram is calculated for each frame.

When to use the 'image per image' method :

* Tend to diminish artefacts
* Gray level histogram erratic

===========
Voxel size
===========

.. image:: Voxel.png
   :align: center
   :alt: alternate text

The voxel size is mandatory for the program's execution.
It will operate when the volume of the object is calculated or during the creation of STL files in order to scale it,
as well as in the STL center of mass calculation.

The size must be filled in millimeter and letters can't be added.
If you fill the first text box (referred to as the X size), all the other text boxes will be automatically filled in with the same value.

In order to activate the execution button, you will first need to fill all the voxel boxes with values above 0.

========
Opening
========

.. image:: Opening.png
   :align: center
   :alt: alternate text

In some cases there are still some isolated pixels on the binarized frames. The goal of the opening methode is to remove these isolated pixels.
By eroding those isolated and dilate the remaining ones.

This is an optional feature and will occur at the end of the segmentation process.
If the opening check box is ticked the option will be executed in the program and its parameters enabled.
The method can be chosen in the combo box, it refers to the mask shape, there is "Ellipse", "Rectangle" and "Cross".
The size of the mask can also be filled.

Example for a 5 per 5 mask size :

.. image:: Opening1.png
   :align: center
   :width: 400
   :alt: alternate text

In order to activate the execution button, you will need to fill all the opening size boxes with values above 0.

============
Borders off
============

.. image:: Borders_off.png
   :align: center
   :alt: alternate text

In tomography, each material has its own density so its own gray level value on the studied frame.
The gray level value of a border is equal to the average of level of gray of its components.
But sometimes the border has the level of gray of an existing component and will appear during the segmentation.

That's why the borders off option was created. To remove those troublesome borders.

This feature will compute after summing the pixels gray level needed for segmentation.
The threshold gradient coefficient goes from 0 to 1 , usually the interesting values are between 0 and 0.1.

Example for a threshold gradient coefficient equal to 0.02 :


.. image:: Borders_off1.png
   :align: center
   :width: 600
   :alt: alternate text

It can be used without any compromise but be careful when studying very small objects, it might cut them out.

For this reason after providing a correct path for your study, it is possible to visualize the effects of this module (visualize the active frame of the picture tab section).

===============
3D Connectivity
===============

.. image:: 3D_Connectivity.png
   :align: center
   :alt: alternate text

The 3D connectivity feature is meant to diminish artefacts or left isolated pixels.
This option will keep the biggest pack of connected pixels of each frame and compare it to the next frame.
If on a picture, a group of isolated pixels is detected, it will be compared with the previous frame.
And if its current coordinates in X/Y are matching with the biggest pack of pixels of the previous image then the pack of isolated pixels is considered as part of the biggest pack.
Otherwise the isolated pixels are deleted.

*******************
Advanced Parameters
*******************

In this tab group box you will be able to set manually the desired calibration for the segmentation.
Select the "Activate manual segmentation" check box and enter the requested parameters.

.. image:: Parameters1.png
   :align: center
   :alt: alternate text

Fill the minimum and maximum level of gray value going from 0 to 65535.
It is not possible to enter letters, a negative value or a minimum value higher than a maximum value.
For this reason please enter the maximum value wanted first as you can only enter the minimum value after.

These border values for the segmentation will be the same for each picture of the images stack.

***********
Object list
***********

.. image:: Object_list.png
   :align: center
   :alt: alternate text

The object list group box is where you refer the different objects you are studying.
If you have several materials such as bronze, iron, stainless steal... you will have to enter them all.

For instance, I am analyzing a set of tomography images depicting an implant and its fixation in a bone.
Those are the following objects entered in this specific order :

* Void (air also has a density, don't add void if your image is entirely filled of materials)
* Bone
* Implant
* Fixation

The spin box number refers to the position of the object in the list.
The object with the smallest density needs to be numbered 0 and the object with the biggest density needs to bear the number of objects entered minus one.

.. image:: Object_list1.png
   :align: center
   :alt: alternate text

You can at anytime, delete an object with the cross next to the object.
Change the name of an object by editing the text box or change its position with the spin box.

The program can be executed as long as there is at least one object with its description.
Two objects can not bear the same name and the numbers of the spin boxes shall start from 0 to n objects minus one, without skipping any value.

*********************
Picture visualization
*********************
.. image:: Picture.png
   :align: center
   :alt: alternate text

In this Picture Tab box you can visualize the images from the input folder filled.
With the first horizontal scroll bar related to the frame number, it is possible to navigate in the stack of frame studied.

By ticking the activate thresholding check box it enables manual thresholding with the minimum and maximum frontier.

.. image:: Picture1.png
   :align: center
   :alt: alternate text

And the save active frame button allows the data-back up of the current frame displayed.


***********
Save frames
***********

.. image:: Save_Frames.png
   :align: center
   :alt: alternate text

In this Save frames group box, you can choose to save or not the analyzed picture in the output folder.
The data-backup of those pictures are not mandatory for the saving of STL files.

It is possible thanks to the checkable combo box to select the specified objects to save.

.. image:: Save_Frames1.png
   :align: center
   :alt: alternate text

And as well to choose the file type output.

********
Save STL
********

.. image:: Save_STL.png
   :align: center
   :alt: alternate text

In this Save STL group box, you can choose to save or not the analyzed picture in the output folder.
The stl-backup of those pictures is mandatory for the display of STL files.

Same as for "Save frames" parameters, it is possible to choose witch object to save in stl format.

.. image:: Save_STL1.png
   :align: center
   :alt: alternate text

If the decimation process check box is ticked, the feature decimation will be activated.
The decimation process is meant to reduce the number of triangles of a stl file and diminish the weight of a file.

Reduction coefficient is the percentage of decimation. The closer the number is to one, the more the number of triangles will be low after decimation.
This number has to be below one and above zero.

The preserve topology option is linked to reduction coefficient.
Sometimes, if the reduction coefficient is too high the decimated stl file will have holes and errors in it.
For the integrity of the stl file, put the preserved topology ticked.

.. image:: Save_STL2.png
   :align: center
   :alt: alternate text

After decimation, the file need to be saved, the checkable combo box has different slots:

* Decimation is the option to save the file after the decimation stl process
* Filling is the option to save the file after filling the hole made from the decimation process (usually active if the preserve topology is not ticked)
* Connectivity is the option to save the file after deleting the non connected triangles of the stl file

*******
Display
*******

.. image:: Display.png
   :align: center
   :alt: alternate text

In this Display group box, you can choose to display saved stl.

Calculate the STL's center of mass and display it on the render with a parametric sphere size.
You can choose up to a maximum of five files for display.

.. image:: Display1.png
   :align: center
   :alt: alternate text

************
Progress bar
************

.. image:: Progress_bar.png
   :align: center
   :alt: alternate text

In order to know the progression of the program these progress bars will appear when the run button is clicked and update automatically during the process.

*******
Execute
*******
.. image:: Execute.png
   :align: center
   :alt: alternate text

The execute group box regroups the button needed for :

* Reset the current inputs
* Check the input parameters
* Run the program
* Exit the GUI

=====
Reset
=====

The reset button clears all the inputs filled by the user.

=====
Check
=====

.. image:: Check.png
   :align: center
   :alt: alternate text

The check button is mainly used to verify if the input information are correctly entered.
If the run button is still disabled after all the parameters are filled, you shall click on the check button for further details.

===
Run
===

The run button allows the execution of the program, it is always disabled if the input parameters are wrong.

====
Exit
====

You can exit the GUI at any time by clicking on the top right red cross or the exit button.



Program
################

*******
Library
*******

Library used : ::

  import os
  import gc
  import sys
  import vtk
  import cv2
  import numba
  import numpy as np
  import matplotlib.pyplot as plt
  from PIL import Image
  from scipy.signal import argrelextrema
  from tqdm import tqdm
  from scipy import interpolate, signal, ndimage
  from numba import jit
  from skimage import measure
  from vtk.util import numpy_support
  from texttable import Texttable
  from PyQt5 import QtCore, QtGui, QtWidgets
  from PyQt5.QtGui import *
  from PyQt5.QtWidgets import *
  from PyQt5.QtCore import *

  from source_ui_mod.phantom_ui_mod import Ui_MainWindow


* *os* to create folders
* *gc* to discard unnecessary data
* *sys* to exit the program
* *vtk* for stl creation, process and display
* *cv2* Open.CV for Erode and Dilate operations
* *numba* to accelerate the speed of the program's execution
* *numpy* for mathematical operations and others
* *matplotlib* to display graphics
* *PIL* to open images from a directory
* *scipy* to create a cubic function from our gray level histogram and filter it to get its extrema
* *tqdm* for visual progression bar
* *mesure* for the connectivity feature
* *texttable* for the mass center values display
* *PyQt5* for Qt Designer implementation
* *Ui_MainWindow* for the Qt GUI designed


.. toctree::
  :maxdepth: 2
  :numbered:
