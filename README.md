PHANTOM (PytHon ANalysis of computed TOMography) 
===============================================

## Documentation

The complete documentation of this project is available [here](https://symmehub.gitlab.io/tomography-project/index.html).

## Requirements 

This program needs a Python installation using Anaconda e.g.

The required modules are gathered in **.yml file**:

If you are not familiar with python virtual environment installation from **.yml file** please follow this [link](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file)

- For Windows users see : *Phantom/environment_dos.yml*

- For Mac/Linux users see : *Phantom/environment_unix.yml*

## Getting start

Go to the folder named **Phantom** and run :
        
        python ./phantom.py

The use of the program is described in attached [documentation](https://symmehub.gitlab.io/tomography-project/index.html).


## Authors

- [Kevin CO](kevinco73800@gmail.com)
- [Pierre VACHER](pvach@univ-smb.fr)
- [Christian ELMO](elmokulc@univ-smb.fr)
